$(document).ready(function () {
  document.title = "Index of file hosting services";
  $("#table").DataTable(
    {
      ajax: "./data.json",
      dom:
        "<'row'<'col-sm-12 col-md-4'l><'col-sm-12 col-md-4 dt-buttons text-center'B><'col-sm-12 col-md-4'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5 entries'i><'col-sm-12 col-md-7'p>>",
      paging: true,
      autoWidth: true,
      buttons: [
        "copyHtml5",
        "csvHtml5",
        "excelHtml5",
        "pdfHtml5",
        "print"
      ],
      responsive: true,
      destroy: true,
      deferRender: true,
      columnDefs: [
        {
          targets: 0,
          render: function (data, type, row, meta) {
            if (type === "display") {
              return (
                '<i class="fa fa-external-link" aria-hidden="true"></i>' +
                $("<a>")
                  .attr("href", data)
                  .attr("target", "_blank")
                  .text(data)
                  .wrap("<div></div>")
                  .parent()
                  .html()
              );
            } else {
              return data;
            }
          }
        },
        {
          targets: 1,
          render: function (data, type, row, meta) {
            if (data === "OK") {
              return (
                $("<span>")
                  .attr("class", "green")
                  .text(data)
                  .wrap("<div></div>")
                  .parent()
                  .html()
              );
            } else {
              return (
                $("<span>")
                  .attr("class", "red")
                  .text(data)
                  .wrap("<div></div>")
                  .parent()
                  .html()
              );
            }
          }
        }
      ]
    }
  );
});