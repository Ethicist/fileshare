# Index of file hosting services

![License badge](https://raster.shields.io/badge/license-The%20Unlicense-lightgrey.png "License badge")

![Thumbnail](thumbnail.png?raw=true "Thumbnail")

Тhis project would also set a good example of using responsive DataTables with extra button plugins on Bootstrap 4 Framework.

## Demo

See in action on [GitLab pages](https://ethicist.gitlab.io/fileshare) or [here](https://fileshare.surge.sh/)
Also, here is CodePen: [https://codepen.io/ethicist/pen/eYZBrZJ](https://codepen.io/ethicist/pen/eYZBrZJ)

## Contributing

Please feel free to submit pull requests.
Bugfixes and simple non-breaking improvements will be accepted without any questions.

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the [LICENSE](LICENSE) file or [unlicense.org](https://unlicense.org).
